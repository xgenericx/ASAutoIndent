import sys
import var

def isvalidext( path ):
    if (path.rpartition('.')[2] == 'apl'
     or path.rpartition('.')[2] == 'as'
     or path.rpartition('.')[2] == 'txt'
     or path.rpartition('.')[2] == 'log'
     or path.rpartition('.')[2] == 'bat'
     or path.rpartition('.')[2] == 'win'):
        return True
    return False

def iscomment( line ):
    if ( line.lstrip(' \t').startswith('.*')
     or line.lstrip(' \t').startswith(';') ):
        return True
    return False

def foundSCR( line ):
    if ( line.lower().find('scr') != -1):
        return line.lower().find('scr')
    return False

def containsSemiColonComment( line ):
    if line.find(';') != -1:
        quotenum = line.partition(';')[0].count('"')
        if int((quotenum+1)/2)==int(quotenum/2):
            return True
    return False
            

def ispreprocessor( line ):
    if ( (line.lstrip(' \t').upper().startswith('#IF DEFINED') )
      or (line.lstrip(' \t').upper().startswith('#ENDIF') )) :
        return True
    return False

def ismoncmd( line ):
    if line.lstrip(' \t').upper().startswith('..MONCMD'):
        return True
    else:
        return False

def isload( line ):
    if line.lstrip(' \t').upper().startswith('..LOAD'):
        return True
    else:
        return False

def isheader( line ):
    if ( line.startswith('.ROBOTDATA')
     or  line.startswith('.PROGRAM')
     or  line.startswith('.SYSDATA')
     or  line.startswith('.END')
     or  line.startswith('.JOINTS')
     or  line.startswith('.REALS')
     or  line.startswith('.AUX')
     or  line.startswith('.INTER')
     or  line.startswith('.STRINGS')) :
        return True
    return False

def isloop( line ):
    if ( line.lstrip(' \t').lower().startswith('for ')
     or  line.lstrip(' \t').lower().startswith('for(')
     or  line.lstrip(' \t').lower().startswith('if ')
     or  line.lstrip(' \t').lower().startswith('if(')
     or  line.lstrip(' \t').lower().startswith('while ')
     or  line.lstrip(' \t').lower().startswith('while(') ):
        return True
    else:
        return False

def iscase( line ):
    if ( line.lstrip(' \t').lower().startswith('case ')
     or  line.lstrip(' \t').lower().startswith('scase ')):
        return True
    return False
     
def iselse( line ):
    if ( line.lstrip(' \t').lower().startswith('elseif ')
      or line.lstrip(' \t').lower().startswith('elseif(')
      or ( line.lstrip(' \t').lower().startswith('else')
       and ( line.lstrip(' \t').lower().partition('else')[2].startswith(';')
        or line.lstrip(' \t').lower().partition('else')[2].startswith('\n')
        or line.lstrip(' \t').lower().partition('else')[2].lstrip(' \t').startswith(';')
        or line.lstrip(' \t').lower().partition('else')[2].lstrip(' \t').startswith('\n') ) ) ) :
            return True
    return False

def isvalue( line ):
    if (( line.lstrip(' \t').lower().startswith('value '))
     or  ( line.lstrip(' \t').lower().startswith('svalue '))
     or  ( line.lstrip(' \t').lower().startswith('any '))) :
        return True
    return False
        
def isend( line ):
    if (line.lstrip(' \t').rstrip(' \t').lower()=='end'
     or  (  line.lstrip(' \t').lower().startswith('end')
       and (  line.lstrip(' \t').lower().partition('end')[2].startswith(';')
        or line.lstrip(' \t').lower().partition('end')[2].startswith('\n')
        or line.lstrip(' \t').lower().partition('end')[2].lstrip(' \t').startswith(';')
        or line.lstrip(' \t').lower().partition('end')[2].lstrip(' \t').startswith('\n') ) ) ) :
         return True
    return False

def islabel( line ):
    if ( ( ':' in line)
     and ( line.lstrip(' \t').rpartition(':')[0].isalnum()
      or line.lstrip(' \t').rpartition(':')[2].lstrip(' \t').startswith(';') 
      or ( line.lstrip(' \t').rpartition(':')[0].partition('_')[0].isalnum()
       and line.lstrip(' \t').rpartition(':')[0].partition('_')[2].isalnum() )
      or ( line.lstrip(' \t').rpartition(':')[0].partition('.')[0].isalnum()
       and line.lstrip(' \t').rpartition(':')[0].partition('.')[2].isalnum() ) ) ):
        return True
    return False
        
