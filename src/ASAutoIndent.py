import sys
import os.path
import win32file
import var
from is_ import *
from write_ import *

def doindent(inputpath, outputpath):
    print(outputpath)
    input = open(inputpath,'r', encoding='Shift-JIS',errors='ignore')
    output = open(outputpath,'w',encoding='Shift-JIS',errors='ignore')
    for line in input:
      if containsSemiColonComment(line):
        if var.strip_comments != 0:
          line = line.partition(';')[0]
      if iscomment(line):
        if var.strip_comments == 0:
          if ( (var.indent_scr != 0) and (foundSCR(line)) ):
            SCRpos = foundSCR(line)
            addspace = 56-SCRpos
            if addspace > 0:
              scrtext = line[SCRpos]+line[SCRpos+1]+line[SCRpos+2]
              commentpart = line.partition(scrtext)
              comment = commentpart[0]+' '*addspace+'SCR'+commentpart[2]
              writeleftjust(output,comment.replace('\t',' '))
            else:
              writeleftjust(output,line)
          else:
            writeleftjust(output,line)
      elif ispreprocessor(line):
        writeleftjust(output,line)
      elif ((ismoncmd(line)) or (isload(line))):
        writeleftjust(output,line)
      else:
        if isheader(line):
            writeheader(output,line)
        elif isloop(line):
            writeloop(output,line)
        elif iscase(line):
            writecase(output,line)
        elif iselse(line):
            writeelse(output,line)
        elif isvalue(line):
            writevalue(output,line)
        elif isend(line):
            writeend(output,line)
        elif islabel(line):
            writeleftjust(output,line)
        else:
            writeline(output,line)
    input.close()
    output.close()
    return
for inputs in sys.argv:
    if (inputs == '-c'):
        var.strip_comments = 1
    if (inputs == '-i'):
        var.indent_scr = 1

for inputs in sys.argv:
    if os.path.isdir(inputs):
      for root, dirs, files in os.walk(inputs):
        for name in files:
          if isvalidext(name):
            inputpath = os.path.join(root,name)
            curdir = os.path.dirname(os.path.abspath(inputs))
            if not os.path.exists(curdir+'\\ASAutoIndent_Output'):
                os.makedirs(curdir+'\\ASAutoIndent_Output')
            outputpath= curdir+'\\ASAutoIndent_Output\\'+root.rpartition(curdir)[2]+'\\'+name
            if not os.path.exists(curdir+'\\ASAutoIndent_Output\\'+root.rpartition(curdir)[2]):
                os.makedirs(curdir+'\\ASAutoIndent_Output\\'+root.rpartition(curdir)[2])
            doindent( inputpath, outputpath)
          else:
            inputpath = os.path.join(root,name)
            curdir = os.path.dirname(os.path.abspath(inputs))
            if not os.path.exists(curdir+'\\ASAutoIndent_Output'):
                os.makedirs(curdir+'\\ASAutoIndent_Output')
            outputpath= curdir+'\\ASAutoIndent_Output\\'+root.rpartition(curdir)[2]+'\\'+name
            if not os.path.exists(curdir+'\\ASAutoIndent_Output\\'+root.rpartition(curdir)[2]):
                os.makedirs(curdir+'\\ASAutoIndent_Output\\'+root.rpartition(curdir)[2])
            print(outputpath)
            win32file.CopyFile (inputpath,outputpath,0)
    elif inputs == '-c' or inputs == '-i':
        pass
    else:
      if isvalidext(inputs):
        inputpath = inputs
        filename = inputpath.rpartition('\\')[2]
        curdir = os.path.dirname(os.path.abspath(inputs))
        if not os.path.exists(curdir+'\\ASAutoIndent_Output'):
            os.makedirs(curdir+'\\ASAutoIndent_Output')
        outputpath= curdir+'\\ASAutoIndent_Output\\'+filename
        doindent( inputpath, outputpath)
      elif inputs != sys.argv[0]:
        inputpath = inputs
        filename = inputpath.rpartition('\\')[2]
        curdir = os.path.dirname(os.path.abspath(inputs))
        if not os.path.exists(curdir+'\\ASAutoIndent_Output'):
            os.makedirs(curdir+'\\ASAutoIndent_Output')
        outputpath= curdir+'\\ASAutoIndent_Output\\'+filename
        print(outputpath)
        win32file.CopyFile (inputpath,outputpath,0)
        
