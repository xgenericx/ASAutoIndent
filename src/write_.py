import sys
import var

def writeheader(output, line ):
    var.case = 0
    var.incase[var.case] = 0
    var.inval = 0
    var.indent = 0
    writeline(output,line)
    var.indent = 1

def writeloop(output, line ):
    writeline(output,line)
    if (not (' goto' in line.lower() or ' return' in line.lower()
      or '\tgoto' in line.lower() or '\treturn' in line.lower()) ):
       var.indent =var.indent + 1
       var.incase[var.case] = var.incase[var.case] + 1

def writecase(output, line ):
    writeline(output,line)
    var.inval = var.inval + 2
    var.indent =var.indent + 1
    var.case = var.case + 1

def writeelse(output, line ):
    var.indent =var.indent - 1
    writeline(output,line)
    if (not (' goto' in line.lower() or ' return' in line.lower()
      or '\tgoto' in line.lower() or '\treturn' in line.lower()) ):
       var.indent =var.indent + 1

def writevalue(output, line ):
    print(var.case)
    var.incase[var.case] = 1
    var.indent =var.indent - 1
    var.inval = var.inval - 1
    writeline(output,line)
    var.inval = var.inval + 1
    var.indent =var.indent + 1

def writeend(output, line ):
    var.indent =var.indent - 1
    if ((var.incase[var.case] == 1 ) and var.case > 0):
       var.case = var.case - 1
       var.inval = var.inval - 2
    var.incase[var.case] = var.incase[var.case] - 1
    writeline(output,line)

def writeleftjust(output, line ):
    output.write(line.strip()+'\n')
    
def writeline(output, line ):
    output.write(var.tab*(2*(var.indent+var.case)-var.inval)+line.strip()+'\n')
